import numpy as np
from poissonDiskSampling import bridsonVariableRadius

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator


def uniformDensity(shape):
    """ Create a radius array with uniform density """
    rad = np.zeros(shape) + 3
    return rad


def sphericalDensity(shape):
    """ Create a radius array with the density radially increasing towards the centre """
    rad = np.zeros(shape)
    for x in range(shape[1]):
        for y in range(shape[0]):
            rad[y, x] = np.sqrt((x-150) ** 2 + (y-150) ** 2)

    # Prettier scaling
    rad = np.sqrt(2*rad)

    # Add some noise
    rand = np.random.normal(1, 0.1, shape[0]*shape[1]).reshape(shape)
    rad = rad * rand / 3

    # Limit the maximum density
    idx = np.where(rad < 2)
    rad[idx] = 2

    return rad


def quartersOfConstantDensity(shape):
    """ Create a radius array consisting of four quarters with different densities """
    rad = np.zeros(shape)

    halfY = int(shape[0]/2)
    halfX = int(shape[1]/2)

    rad[0:halfY,0:halfX] = 2
    rad[0:halfY,halfX:] = 4
    rad[halfY:,0:halfX] = 6
    rad[halfY:,halfX:] = 8

    # Add some noise
    rand = np.random.normal(1, 0.1, shape[0]*shape[1]).reshape(shape)
    rad = rad * rand

    return rad


def chessDensity(shape):
    """ Create a radius array resembling a chessboard pattern """
    rad = np.zeros(shape)
    for i in range(5):
        for o in range(5):
            if (o + i*5)%2 == 0:
                rad[60*i:60*(i+1), 60*o:60*(o+1)] = 3
            else:
                rad[60*i:60*(i+1), 60*o:60*(o+1)] = 5

    # Add some noise
    rand = np.random.normal(1, 0.1, shape[0]*shape[1]).reshape(shape)
    rad = rad * rand

    return rad


def plotSampling(particleCoordinates):
    """ Plot the density map and resulting Poisson Disk Sampling. """
    fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10,5))
    ax1.set_title("Density Field")
    ax1.imshow(rad, cmap='RdBu', interpolation='none')
    ax2.set_title("Poisson Disk Sampling")
    ax2.scatter(particleCoordinates[:,0], particleCoordinates[:,1], marker='.', s=4, c='k')

    ax1.xaxis.set_major_locator(MultipleLocator(50))
    ax1.yaxis.set_major_locator(MultipleLocator(50))
    ax1.xaxis.set_minor_locator(MultipleLocator(25))
    ax1.yaxis.set_minor_locator(MultipleLocator(25))
    ax1.tick_params(direction="in", which='both', bottom=True, top=True, left=True, right=True)
    ax2.tick_params(direction="in", which='both', bottom=True, top=True, left=True, right=True)
    ax1.set_xlim([0,300])
    ax1.set_ylim([0,300])

    fig.tight_layout(rect=[0, 0.03, 1, 0.97])
    plt.savefig("uniformDensity.png")


if __name__ == "__main__":
    """
    This file presents various examples on the usage of this software package and the generated Poisson Disk Samplings. 
    """

    # Create the Radius Array
    rad = uniformDensity((300,300))            # Uniform sampling radius throughout the field
    # rad = sphericalDensity((300,300))          # Radially decreasing sampling radius
    # rad = quartersOfConstantDensity((300,300)) # Four quarters with different densities (run with k=100)
    # rad = chessDensity((300,300))              # A chessboard pattern of different densities (run with k=100)

    # Run the Poisson Disk Sampling
    nParticle, particleCoordinates = bridsonVariableRadius.poissonDiskSampling(rad, k=30, radiusType='default')

    # Visualise the resulting sampling
    plotSampling(particleCoordinates)
